#include <Wire.h>
#include <LiquidCrystal_I2C.h>
 
#define PIN_BOTAO 2
#define PIN_BOTAO_INVALIDAR 6
#define PIN_INICIO 4
#define PIN_FIM 5
 
LiquidCrystal_I2C lcd(0x27, 20, 4);
float ts;
bool flag = false;
 
void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(PIN_BOTAO, INPUT_PULLUP);
  pinMode(PIN_INICIO, INPUT);
  pinMode(PIN_FIM, INPUT);
  lcd.init();
  lcd.backlight();
  lcd.setCursor(2, 0);
  lcd.print("CRONOMETRO V 1.0");
 
}
 
void loop() {
  // put your main code here, to run repeatedly:
  lcd.setCursor(0, 2);
  Serial.println(digitalRead(PIN_BOTAO));;
  Serial.println(digitalRead(PIN_BOTAO_INVALIDAR));;
  
  Serial.println(analogRead(PIN_INICIO));
  delay(100);
    lcd.print("INICIO=");
  if (digitalRead(PIN_INICIO)) {
    lcd.print("OK  ");
  } else {
    lcd.print("ERRO");
  }
  lcd.setCursor(12, 2);
  lcd.print("FIM=");
  if (digitalRead(PIN_FIM)) {
    lcd.print("OK  ");
  } else {
    lcd.print("ERRO");
  }
 
 
if (digitalRead(PIN_BOTAO_INVALIDAR)) {
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("INVALIDO!");
    lcd.setCursor(0, 2);
    flag = true;
  }
  while (flag) {
    bool inicio = digitalRead(PIN_INICIO);
    bool fim = digitalRead(PIN_FIM);
 
    long tempo =  millis();
    if (!inicio) {
      lcd.clear();
      while (fim) {
        lcd.setCursor(2, 0);
        lcd.print("CRONOMETRO V 1.0");
        fim = digitalRead(PIN_FIM);
        lcd.setCursor(0, 2);
        lcd.print("TEMPO=");
        ts = (float) (millis() - tempo) / 1000;
        lcd.print(ts, 4);
      }
      lcd.clear();
      while (digitalRead(PIN_BOTAO)) {
        lcd.setCursor(1, 0);
        lcd.print("CORRIDA CONCLUIDA!");
        lcd.setCursor(0, 2);
        lcd.print("TEMPO=");
        lcd.print(ts, 4);
      }
      while (!digitalRead(PIN_BOTAO_INVALIDAR)) {
        lcd.setCursor(1, 0);
        lcd.print("INVALIDADA!");
        lcd.setCursor(0, 2);
        lcd.print("TEMPO=");
        lcd.print(ts, 4);
      }
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print("CRONOMETRO V 1.0");
      flag = false;
      delay(500);
    }
  }
}
